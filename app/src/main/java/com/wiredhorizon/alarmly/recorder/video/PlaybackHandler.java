package com.wiredhorizon.alarmly.recorder.video;


public interface PlaybackHandler {
	public void onPreparePlayback();
}

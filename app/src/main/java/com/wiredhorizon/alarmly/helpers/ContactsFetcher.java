package com.wiredhorizon.alarmly.helpers;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.wiredhorizon.alarmly.models.Contact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by chriszhu on 15-01-07.
 */
public class ContactsFetcher extends AsyncTask<Void, Void, List<Contact>> {
    private Context context;
    private ArrayList<Contact> contacts;
    private ContactsTranslatorService.ContactsFetchedCallback callback;

    public ContactsFetcher(Context context, ContactsTranslatorService.ContactsFetchedCallback callback) {
        this.context = context;
        this.contacts = new ArrayList<Contact>();
        this.callback = callback;
    }

    @Override
    protected ArrayList<Contact> doInBackground(Void... params) {
        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        while (phones.moveToNext())
        {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phoneNumber = phoneNumber.replaceAll("\\D+","");
            contacts.add(new Contact(name, phoneNumber));
        }

        phones.close();
        Collections.sort(contacts);
        return contacts;
    }

    @Override
    protected void onPostExecute(List<Contact> contacts) {
        callback.callback(contacts);
    }
}

package com.wiredhorizon.alarmly.helpers;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.wiredhorizon.alarmly.Constants;
import com.wiredhorizon.alarmly.api.FileUploader;
import com.wiredhorizon.alarmly.api.dao.StatusDAO;

import java.io.File;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by chriszhu on 14-12-12.
 */
public class VideoUploader {
    private Context context;
    public VideoUploader(Context context) {
        this.context = context;
    }

    public void upload(String path, int alarmId, Callback<StatusDAO> callback) {
        String mimeType = getMimeType(path);

        TypedFile typedFile = new TypedFile(mimeType, new File(path));
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(Constants.UPLOAD).build();
        FileUploader uploader = restAdapter.create(FileUploader.class);
        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        uploader.upload(accessToken, alarmId ,typedFile, "video", callback);
    }

    public static String getMimeType(String url)
    {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }
}

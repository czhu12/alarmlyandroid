package com.wiredhorizon.alarmly.helpers;

import android.content.Context;
import android.util.Log;

import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.models.Contact;
import com.wiredhorizon.alarmly.models.User;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 15-01-07.
 */
public class ContactsTranslatorService {
    private Context context;
    public ContactsTranslatorService(Context context) {
        this.context = context;
    }

    public void getContactsList(ContactsFetchedCallback callback) {
        ContactsFetcher contactsFetcher = new ContactsFetcher(context, callback);
        contactsFetcher.execute();
    }

    public void translate(final Callback<List<User>> callback) {
        ContactsFetcher contactsFetcher = new ContactsFetcher(context, new ContactsFetchedCallback() {
            @Override
            public void callback(final List<Contact> contacts) {
                // We get a list of contacts

                AlarmlyAPI api = AlarmlyAPI.getInstance();
                //api.translateContacts(context, contacts, callback);
                api.translateContacts(context, contacts, new Callback<List<User>>() {
                    @Override
                    public void success(List<User> users, Response response) {
                        // We need to translate these users from the server (with no name)
                        // against the contacts we have.
                        List<User> usersWithNames = getUserNames(users, contacts);
                        callback.success(usersWithNames, response);
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        callback.failure(retrofitError);
                    }
                });
            }
        });

        contactsFetcher.execute();
    }
    /*
     * This method will take a list of users that are translated from the server.
     * Compare their phone number to the full list of contacts.
     * If one is found, then it will give the user the name of the contact.
     */
    public List<User> getUserNames(List<User> users, List<Contact> contacts) {

        List<User> usersWithNames = new ArrayList<User>();
        for (User user : usersWithNames) {

            String userPhoneNumber = user.getPhoneNumber();
            Contact contact = findContact(userPhoneNumber, contacts);
            if (contact == null) {
                Log.i("ContactsTranslatorService", "translation error: contact not found");
            } else {
                user.setName(contact.getName());
                usersWithNames.add(user);
            }
        }

        return usersWithNames;
    }

    public Contact findContact(String userPhoneNumber, List<Contact> contacts) {
        for (int i = 0; i < contacts.size(); i++) {
            Contact contact = contacts.get(i);
            if (contact.comparePhoneNumber(userPhoneNumber)) {
                return contact;
            }
        }

        return null;
    }


    public interface ContactsFetchedCallback {
        public void callback(List<Contact> contacts);
    }
}

package com.wiredhorizon.alarmly.helpers;

/**
 * Created by chriszhu on 14-12-23.
 */
public interface GenericCallback {
    public void callback();
    public void callback(String string);
}
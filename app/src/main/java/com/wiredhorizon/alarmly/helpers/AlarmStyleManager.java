package com.wiredhorizon.alarmly.helpers;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.models.Alarm;

/**
 * Created by chriszhu on 14-12-26.
 */
public class AlarmStyleManager {
    private Context context;
    public AlarmStyleManager(Context context) {
        this.context = context;
    }

    public int getColor(Alarm alarm) {
        int militaryHour = alarm.getMilitaryHour();
        switch (militaryHour) {
            case 0:
            case 1:
                return getColor(R.color.wisteria);
            case 2:
            case 3:
                return getColor(R.color.wet_asphalt);
            case 4:
            case 5:
                return getColor(R.color.midnight_blue);
            case 6:
            case 7:
                return getColor(R.color.alizarin);
            case 8:
            case 9:
                return getColor(R.color.pumpkin);
            case 10:
            case 11:
                return getColor(R.color.orange);
            case 12:
            case 13:
                return getColor(R.color.sun_flower);
            case 14:
            case 15:
                return getColor(R.color.emerald);
            case 16:
            case 17:
                return getColor(R.color.turquoise);
            case 18:
            case 19:
                return getColor(R.color.peter_river);
            case 20:
            case 21:
                return getColor(R.color.belize_hole);
            case 22:
            case 23:
                return getColor(R.color.amethyst);
            default:
                return 0;
        }
    }

    private int getColor(int colorRes) {
        return context.getResources().getColor(colorRes);
    }

    private Drawable getDrawable(int drawableRes) {
        return context.getResources().getDrawable(drawableRes);
    }
    public Drawable getDayIcon(Alarm alarm) {
        int militaryHour = alarm.getMilitaryHour();
        if (militaryHour >= 6 && militaryHour <= 9) {
            return getDrawable(R.drawable.ic_sun_rise);
        } else if (militaryHour > 9 && militaryHour < 22) {
            return getDrawable(R.drawable.ic_sun);
        } else {
            return getDrawable(R.drawable.ic_moon);
        }
    }
}

package com.wiredhorizon.alarmly.helpers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.wiredhorizon.alarmly.models.Alarm;
import com.wiredhorizon.alarmly.services.AlarmService;

/**
 * Created by chriszhu on 14-11-26.
 */
public class AlarmScheduler {
    public static boolean scheduleAlarm(Context context, Alarm alarm) {
        Intent i = new Intent(context, AlarmService.class);
        i.setAction(AlarmService.CREATE);
        i.putExtra(AlarmService.ALARM_ID, alarm.getId());
        context.startService(i);
        return true;
    }

    public static boolean disableAlarm(Context context, Alarm alarm) {
        Intent i = new Intent(context, AlarmService.class);
        i.setAction(AlarmService.CANCEL);
        i.putExtra(AlarmService.ALARM_ID, alarm.getId());
        context.startService(i);
        return true;
    }
}

package com.wiredhorizon.alarmly.helpers;

/**
 * Created by Chris on 12/6/2014.
 */

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.gson.Gson;
import com.wiredhorizon.alarmly.Constants;
import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.models.User;
import com.wiredhorizon.alarmly.services.PreferencesManager;
import com.wiredhorizon.alarmly.system.DeviceManager;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 8/28/14.
 */
public class AuthenticationHelper {
    public static final String USER_CHANNEL = "user_channel";

    public static void login(Context context, User u) {
        saveUser(context, u);
    }

    public static void saveUser(Context context, User u) {
        String serializedUser = u.toJSON();
        PreferencesManager.getInstance().put(context, Constants.CURRENT_USER, serializedUser);
    }

    public static String getAlarmlyAccessToken(Context context) {
        User currentUser = getCurrentUser(context);
        return currentUser.accessToken;
    }

    public static boolean isLoggedIn(Context context) {
        PreferencesManager prefMan = PreferencesManager.getInstance();
        return prefMan.contains(context, Constants.CURRENT_USER);
    }

    public static User getCurrentUser(Context context) {
        String serializedUser = PreferencesManager.getInstance().get(context, Constants.CURRENT_USER);
        User currentUser = null;
        Gson gson = new Gson();
        currentUser = gson.fromJson(serializedUser, User.class);

        return currentUser;
    }

    public static int getCurrentUserID(Context context) {
        User currentUser = getCurrentUser(context);
        return currentUser.id;
    }

    public static String showCurrentUser(Context context) {
        Gson gson = new Gson();
        if (isLoggedIn(context)) {
            String currentUser = gson.toJson(getCurrentUser(context));
            Log.i("CURRENT USER", currentUser);
        }

        return "";
    }

    public static void createUser(Context context, Callback<User> callback) {
        DeviceManager manager = new DeviceManager();
        String phoneNumber = manager.getPhoneNumber(context);
        String deviceID = manager.getDeviceID(context);

        User user = new User(phoneNumber, deviceID);
        AlarmlyAPI api = AlarmlyAPI.getInstance();
        api.createUser(user, callback);
    }
}
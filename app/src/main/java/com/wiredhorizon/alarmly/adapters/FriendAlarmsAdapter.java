package com.wiredhorizon.alarmly.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.models.Alarm;
import com.wiredhorizon.alarmly.models.User;

import java.util.List;

/**
 * Created by chriszhu on 14-12-11.
 */
public class FriendAlarmsAdapter extends ArrayAdapter<Alarm> {
    public FriendAlarmsAdapter(Context context, int resource, List<Alarm> alarms) {
        super(context, resource, alarms);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_friend_alarms_list, null);
        }

        ViewHolder holder = (ViewHolder) v.getTag(R.id.id_holder);

        if (holder == null) {
            holder = new ViewHolder(v);
            v.setTag(R.id.id_holder, holder);
        }

        Alarm alarm = getItem(position);
        v.setTag(R.id.alarm_holder, alarm);

        holder.title.setText(alarm.getReadableTime());

        return v;
    }

    class ViewHolder {
        TextView title;
        public ViewHolder(View v) {
            title = (TextView) v.findViewById(R.id.row_friend_alarms_title);
        }
    }
}

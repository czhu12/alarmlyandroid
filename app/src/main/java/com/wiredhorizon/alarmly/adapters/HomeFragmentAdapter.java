package com.wiredhorizon.alarmly.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.fragments.AlarmsFragment;
import com.wiredhorizon.alarmly.fragments.FriendAlarmsFragment;
import com.wiredhorizon.alarmly.fragments.FriendsFragment;

import java.util.Locale;

/**
 * Created by chriszhu on 14-12-11.
 */


public class HomeFragmentAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private Fragment currentFriendsFragment;
    private FragmentManager mFragmentManager;


    public HomeFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
        mFragmentManager = fm;
    }

    /*
    @Override
    public int getItemPosition(Object object) {
        if (object instanceof FriendsFragment && currentFriendsFragment instanceof FriendAlarmsFragment)
            return POSITION_NONE;

        if (object instanceof FriendAlarmsFragment && currentFriendsFragment instanceof FriendsFragment)
            return POSITION_NONE;

        return POSITION_UNCHANGED;
    }
    */

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position) {
            case 0:
                return AlarmsFragment.newInstance();
            case 1:
                return getAlarmFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return mContext.getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return mContext.getString(R.string.title_section2).toUpperCase(l);
        }
        return null;
    }
    /*
    private Fragment getAlarmFragment() {
        if (currentFriendsFragment == null) {
            currentFriendsFragment = FriendsFragment.newInstance(new FriendsFragmentListener() {
                @Override
                public void onSwitchToNextFragment() {
                    mFragmentManager.beginTransaction().remove(currentFriendsFragment).commit();
                    currentFriendsFragment = FriendAlarmsFragment.newInstance();
                    notifyDataSetChanged();
                }
            });
        }
        return currentFriendsFragment;
    }
    */
    private Fragment getAlarmFragment() {
        return FriendsFragment.newInstance();
    }


    public interface FriendsFragmentListener
    {
        void onSwitchToNextFragment();
    }
}
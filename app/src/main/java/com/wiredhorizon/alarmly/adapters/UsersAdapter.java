package com.wiredhorizon.alarmly.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.models.User;

import java.util.List;

/**
 * Created by Chris on 12/18/2014.
 */
public class UsersAdapter extends ArrayAdapter<User> {
    public UsersAdapter(Context context, int resource, List<User> objects) {
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_users_list, null);
        }

        ViewHolder holder = (ViewHolder) v.getTag(R.id.id_holder);

        if (holder == null) {
            holder = new ViewHolder(v);
            v.setTag(R.id.id_holder, holder);
        }

        User user = getItem(position);
        v.setTag(R.id.user_holder, user);


        holder.userName.setText(user.phoneNumber);

        return v;
    }

    class ViewHolder {
        TextView userName;
        public ViewHolder(View v) {
            userName = (TextView) v.findViewById(R.id.row_users_list_phone_number);
        }
    }
}

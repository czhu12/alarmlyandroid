package com.wiredhorizon.alarmly.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.helpers.AlarmStyleManager;
import com.wiredhorizon.alarmly.models.Alarm;

import java.util.List;

/**
 * Created by chriszhu on 14-11-23.
 */
public class AlarmsAdapter extends ArrayAdapter<Alarm> {
    private AlarmStyleManager alarmStyleManager;
    public AlarmsAdapter(Context context, int resource, List<Alarm> objects) {
        super(context, resource, objects);
        alarmStyleManager = new AlarmStyleManager(getContext());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_alarms_list, null);
        }

        ViewHolder holder = (ViewHolder) v.getTag(R.id.id_holder);

        if (holder == null) {
            holder = new ViewHolder(v);
            v.setTag(R.id.id_holder, holder);
        }

        final Alarm alarm = getItem(position);
        holder.alarmMeridiem.setText(alarm.getMeridiem());
        holder.alarmTime.setText(alarm.getReadableTime());
        holder.alarmTitle.setText(alarm.getTitle());
        holder.alarmDayIcon.setImageDrawable(alarmStyleManager.getDayIcon(alarm));
        if (alarm.isActive()) {
            holder.alarmToggle.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_alarm_on));
        } else {
            holder.alarmToggle.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_alarm_off));
        }

        final ViewHolder innerHolder = holder;

        holder.alarmToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alarm.isActive()) {
                    innerHolder.alarmToggle.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_alarm_off));
                } else {
                    innerHolder.alarmToggle.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_alarm_on));
                }

                boolean isActive = alarm.isActive();
                alarm.setActive(!isActive);
                alarm.saveAndSetAlarm(getContext());
            }
        });

        v.setBackgroundColor(alarmStyleManager.getColor(alarm));

        return v;
    }

    class ViewHolder {
        TextView alarmTitle;
        TextView alarmTime;
        TextView alarmMeridiem;
        ImageView alarmToggle;
        ImageView alarmDayIcon;

        public ViewHolder(View v) {
            alarmTitle = (TextView) v.findViewById(R.id.row_alarms_list_title);
            alarmTime = (TextView) v.findViewById(R.id.row_alarms_list_time);
            alarmMeridiem = (TextView) v.findViewById(R.id.row_alarms_list_meridiem);
            alarmToggle = (ImageView) v.findViewById(R.id.row_alarms_list_toggle);
            alarmDayIcon = (ImageView) v.findViewById(R.id.row_alarms_list_day_icon);
        }
    }
}

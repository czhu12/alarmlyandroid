package com.wiredhorizon.alarmly.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.models.User;

import java.util.List;

/**
 * Created by chriszhu on 14-11-23.
 */
public class FriendsAdapter extends ArrayAdapter<User> {
    public FriendsAdapter(Context context, int resource, List<User> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_friends_list, null);
        }

        ViewHolder holder = (ViewHolder) v.getTag(R.id.id_holder);

        if (holder == null) {
            holder = new ViewHolder(v);
            v.setTag(R.id.id_holder, holder);
        }

        User user = getItem(position);
        v.setTag(R.id.user_holder, user);


        holder.userName.setText(user.getPhoneNumber());

        return v;
    }

    class ViewHolder {
        TextView userName;
        public ViewHolder(View v) {
            userName = (TextView) v.findViewById(R.id.row_friends_list_name);
        }
    }
}

package com.wiredhorizon.alarmly.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chris on 12/18/2014.
 */
public class AlarmMessage {
    @SerializedName("id") public int id;
    @SerializedName("format") public String format;
    @SerializedName("link") public String link;
    @SerializedName("message") public String message;
    @SerializedName("viewed") public boolean viewed;

    public AlarmMessage() {}

    public boolean isVideo () {
        return format.equals("video");
    }

    public boolean isPicture () {
        return format.equals("picture");
    }

    public boolean isMessage () {
        return format.equals("message");
    }

    public String getFormat() {
        return format;
    }
}

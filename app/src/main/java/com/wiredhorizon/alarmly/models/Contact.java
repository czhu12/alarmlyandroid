package com.wiredhorizon.alarmly.models;

/**
 * Created by chriszhu on 15-01-07.
 */
public class Contact implements Comparable {
    private String name;
    private String phoneNumber;

    public Contact(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean comparePhoneNumber(String userPhoneNumber) {
        return false;
    }
}

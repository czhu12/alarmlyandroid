package com.wiredhorizon.alarmly.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

/**
 * Created by Chris on 12/9/2014.
 */


public class User implements Parcelable {
    @SerializedName("id")
    public int id;

    @SerializedName("access_token")
    public String accessToken;

    @SerializedName("phone_number")
    public String phoneNumber;

    @SerializedName("device_id")
    public String deviceID;

    private String name;


    public User() {}

    public User(String phoneNumber, String deviceID) {
        this.phoneNumber = phoneNumber;
        this.deviceID = deviceID;
    }

    public String toJSON() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static User fromJSON(JSONObject userSerialized) {
        Gson gson = new Gson();

        User user = gson.fromJson(userSerialized.toString(), User.class);
        return user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(accessToken);
        dest.writeString(name);
    }

    public User(Parcel parcel) {
        id = parcel.readInt();
        accessToken = parcel.readString();
        name = parcel.readString();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
package com.wiredhorizon.alarmly.models;

import android.content.Context;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.SerializedName;
import com.wiredhorizon.alarmly.api.AlarmDAO;
import com.wiredhorizon.alarmly.helpers.AlarmScheduler;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by chriszhu on 14-11-23.
 */
@Table(name = "Alarms")
public class Alarm extends Model implements Comparable<Alarm>{
    public static final String AM = "AM";
    public static final String PM = "PM";
    @Column(name="remoteId")
    private int remoteId;

    @Column(name="title")
    private String mTitle;

    @Column(name="minute")
    private int mMilitaryMinute;

    @Column(name="hour")
    private int mMilitaryHour;

    @Column(name="active")
    private boolean mActive;

    @Column(name="is_synced")
    private boolean mIsSynced;

    @Column(name="updated_at")
    private Date mUpdatedAt;

    @Column(name="is_deleted")
    private boolean mIsDeleted;

    public Alarm() {}

    public Alarm(int remoteId, String title, int militaryHour, int militaryMinute, boolean active) {
        mTitle = title;
        mMilitaryHour = militaryHour;
        mMilitaryMinute = militaryMinute;
        mActive = active;
        mIsSynced = false;
        mIsDeleted = false;
        this.remoteId = remoteId;
        refreshValues();
    }
    public Alarm(String title, int militaryHour, int militaryMinute, boolean active) {
        mTitle = title;
        mMilitaryHour = militaryHour;
        mMilitaryMinute = militaryMinute;
        mActive = active;
        mIsSynced = false;
        mIsDeleted = false;
        refreshValues();
    }

    public Alarm(String title, int militaryHour, int militaryMinute, boolean active, boolean isSynced, Date updatedAt) {
        mTitle = title;
        mMilitaryHour = militaryHour;
        mMilitaryMinute = militaryMinute;
        mActive = active;
        mUpdatedAt = updatedAt;
        mIsSynced = isSynced;
    }

    public static List<Alarm> index() {
        return new Select().from(Alarm.class).where("is_deleted = '0'").execute();
    }

    public static Alarm find(long alarmID) {
        return new Select().from(Alarm.class).where("Id = ?", alarmID).executeSingle();
    }

    public int getMinute() {
        return mMilitaryMinute;
    }

    public int getHour() {
        int hour = mMilitaryHour % 12;
        if (hour == 0)
            return 12;

        return hour;
    }

    public boolean isActive() {
        return mActive;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getMeridiem() {
        if (mMilitaryHour <= 12) {
            return Alarm.AM;
        }
        return Alarm.PM;
    }

    public String getReadableTime() {
        int minute = getMinute();
        String minuteString = String.valueOf(minute);
        if (minuteString.length() == 1) {
            minuteString = "0" + minuteString;
        }

        return getHour() + ":" + minuteString;
    }

    public int getMilitaryMinute() {
        return mMilitaryMinute;
    }

    public int getMilitaryHour() {
        return mMilitaryHour;
    }

    public static Alarm fromMilitaryTime(String title, int militaryHour, int militaryMinute, boolean active) {
        return new Alarm(title, militaryHour, militaryMinute, active);
    }

    public void setActive(boolean active) {
        mActive = active;
    }

    public void updateWithMilitaryTime(Integer currentHour, Integer currentMinute) {
        refreshValues();
        mMilitaryHour = currentHour;
        mMilitaryMinute = currentMinute;
    }

    public Long saveAndSetAlarm(Context context) {
        refreshValues();

        long id = this.save();

        if (this.isActive() && !this.isDeleted()) {
            Log.i("Alarm Scheduling", "Scheduled alarm for :" + this.getReadableTime());
            AlarmScheduler.scheduleAlarm(context, this);
        } else {
            Log.i("Alarm Scheduling", "Unscheduled alarm for :" + this.getReadableTime());
            AlarmScheduler.disableAlarm(context, this);
        }

        return id;
    }

    public void refreshValues() {
        this.mIsSynced = false;
        this.mUpdatedAt = new Date(System.currentTimeMillis());
    }

    public static List<Alarm> getAlarmsToSync() {
        return new Select().from(Alarm.class).where("is_synced = 0").execute();
    }

    public AlarmDAO toAlarmDAO() {
        return new AlarmDAO(mTitle, mMilitaryHour, mMilitaryMinute, mActive, mIsSynced, mUpdatedAt, getId());
    }

    public void setRemoteId(int remoteId) {
        this.remoteId = remoteId;
        this.mUpdatedAt = new Date(System.currentTimeMillis());
        save();
    }

    public boolean existsRemotely() {
        return remoteId != 0;
    }

    public void setIsSynced(boolean isSynced) {
        this.mIsSynced = isSynced;
    }

    public long getUpdatedAt() {
        return mUpdatedAt.getTime();
    }

    public int getRemoteId() {
        return remoteId;
    }

    public boolean isSynced() {
        return mIsSynced;
    }

    public boolean isDeleted() {
        return mIsDeleted;
    }

    public void scheduleForDelete(Context context) {
        refreshValues();
        this.mIsDeleted = true;
        saveAndSetAlarm(context);
    }

    public static void printSync() {
        List<Alarm> alarms = getAlarmsToSync();
        for (Alarm alarm: alarms) {
            Log.i("Need to sync alarm:", alarm.toString());
        }
    }

    @Override
    public String toString() {
        return getTitle() + " "
                + getMilitaryHour() + ":" + getMilitaryMinute()
                + " active: " + isActive()
                + " deleted " + isDeleted();
    }

    public static void sortAlarms(List<Alarm> unsortedAlarms) {
        Collections.sort(unsortedAlarms);
    }

    @Override
    public int compareTo(Alarm alarm) {
        if (getMilitaryHour() > alarm.getMilitaryHour()) return 1;

        if (getMilitaryHour() < alarm.getMilitaryHour()) return -1;

        if (getMilitaryMinute() > alarm.getMilitaryMinute()) return 1;

        if (getMilitaryMinute() < alarm.getMilitaryMinute()) return -1;


        return 0;
    }
}
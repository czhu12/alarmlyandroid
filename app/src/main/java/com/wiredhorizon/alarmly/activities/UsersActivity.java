package com.wiredhorizon.alarmly.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.adapters.UsersAdapter;
import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Chris on 12/16/2014.
 */
public class UsersActivity extends Activity {
    @InjectView(R.id.users_list)
    ListView usersListView;

    UsersAdapter usersAdapter;
    ArrayList<User> usersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        ButterKnife.inject(this);
        initializeListView();
        fetchUsers();
    }

    private void initializeListView() {
        usersList = new ArrayList<User>();
        usersAdapter = new UsersAdapter(this, 0, usersList);
        usersListView.setAdapter(usersAdapter);
        usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // Here, we want to make them a friend.
                User friend = (User) view.getTag(R.id.user_holder);
                addFriend(friend);
            }
        });
    }

    private void addFriend(User friend) {
        AlarmlyAPI api = AlarmlyAPI.getInstance();
        api.addFriend(this, friend.id, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                // Now what? We want to notify the user that the person has been added as a friend.
                finish();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i("UsersActivity", retrofitError.getMessage() == null ? "Error adding friend" : retrofitError.getMessage());
            }
        });
    }

    public void fetchUsers() {
        AlarmlyAPI api = AlarmlyAPI.getInstance();
        api.getUsers(this, new Callback<List<User>>() {
            @Override
            public void success(List<User> users, Response response) {
                usersList.clear();
                usersList.addAll(users);
                usersAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i("UsersActivity", retrofitError.getMessage() == null ? "Error fetching users" : retrofitError.getMessage());
            }
        });
    }
}

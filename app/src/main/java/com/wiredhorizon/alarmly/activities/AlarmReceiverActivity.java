package com.wiredhorizon.alarmly.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.wiredhorizon.alarmly.R;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Chris on 11/27/2014.
 */
public class AlarmReceiverActivity extends Activity {
    public static final String ALARM_ID = "ALARM_ID";
    private long alarmId;

    private static final String WAKE_LOCK_KEY = "WAKE_LOCK_KEY";

    private MediaPlayer mMediaPlayer;
    PowerManager.WakeLock mWakeLock;

    @InjectView(R.id.activity_alarm_receiver_stop)
    protected Button stopButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("Inside alarm receiver", "Hi");
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, WAKE_LOCK_KEY);

        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        );
        setContentView(R.layout.activity_alarm_receiver);
        ButterKnife.inject(this);
        alarmId = getIntent().getLongExtra(ALARM_ID, 0);

        stopButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMediaPlayer.stop();
                if (mWakeLock.isHeld()) {
                    mWakeLock.release();
                }

                Intent i = new Intent(getBaseContext(), PlayVideoActivity.class);
                i.putExtra(PlayVideoActivity.ALARM_ID, alarmId);
                startActivity(i);
            }
        });
        playAlarmSound(getAlarmUri());
    }

    private void playAlarmSound(Uri alert) {
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(this, alert);
            final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IOException e) {
        }
    }

    private Uri getAlarmUri() {
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }

        return alert;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }
    }
}

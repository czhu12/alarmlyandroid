package com.wiredhorizon.alarmly.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.api.dao.StatusDAO;
import com.wiredhorizon.alarmly.helpers.VideoUploader;
import com.wiredhorizon.alarmly.recorder.video.AdaptiveSurfaceView;
import com.wiredhorizon.alarmly.recorder.video.PlaybackHandler;
import com.wiredhorizon.alarmly.recorder.video.VideoPlaybackManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 14-12-15.
 */
public class PreviewVideoActivity extends Activity {
    public static final String FILE_PATH = "FILE_PATH";
    private String filePath;
    @InjectView(R.id.video_view)
    VideoView videoView;

    @InjectView(R.id.send_button)
    Button sendButton;

    @InjectView(R.id.cancel_button)
    Button cancelButton;

    private int alarmId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_video);

        Intent i = getIntent();

        if ((i != null) && (i.getExtras() != null)) {
            filePath = i.getExtras().getString(FILE_PATH);
            alarmId = i.getExtras().getInt(CameraActivity.ALARM_ID, 0);
        }

        ButterKnife.inject(this);
        setUpVideoPlayback();
    }

    private void setUpVideoPlayback() {
        Uri videoUri = Uri.parse(filePath);
        videoView.setVideoURI(videoUri);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoView.start();
            }
        });

        videoView.start();
    }

    @OnClick(R.id.send_button)
    protected void sendButtonClicked() {
        sendButton.setEnabled(false);

        VideoUploader uploader = new VideoUploader(this);
        uploader.upload(filePath, alarmId, new Callback<StatusDAO>() {
            @Override
            public void success(StatusDAO dao, Response response) {
                Log.i("Status", "Video successfully uploaded");
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Toast.makeText(getBaseContext(), "Video Successfully Uploaded", Toast.LENGTH_SHORT).show();
                startActivity(i);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String message = retrofitError.getMessage() == null ? "" : retrofitError.getMessage();
                Log.i("Status", "Video upload failed " + message);
                sendButton.setEnabled(true);
            }
        });
    }

    @OnClick(R.id.cancel_button)
    protected void cancelButtonClicked() {
        finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

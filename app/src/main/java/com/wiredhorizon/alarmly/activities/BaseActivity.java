package com.wiredhorizon.alarmly.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.wiredhorizon.alarmly.Constants;
import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.helpers.AuthenticationHelper;
import com.wiredhorizon.alarmly.models.User;
import com.wiredhorizon.alarmly.services.AlarmService;
import com.wiredhorizon.alarmly.system.DeviceManager;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Chris on 12/9/2014.
 */
public abstract class BaseActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Constants.SHOULD_VERIFY_LOGGED_IN) {
            verifyLoginOrCreate();
        }

        if (AuthenticationHelper.isLoggedIn(getBaseContext())) {
            AlarmService.rescheduleInterval(getBaseContext());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.SHOULD_VERIFY_LOGGED_IN) {
            verifyLoginOrCreate();
        }
    }

    private void verifyLoginOrCreate() {
        Log.i("Got here","checking login");
        boolean isNetworkConnected = new DeviceManager().isNetworkConnected(getBaseContext());
        if (!AuthenticationHelper.isLoggedIn(this) && isNetworkConnected) {
            startActivity(new Intent(this, StallActivity.class));
            AlarmlyAPI.disableUserRequests();
        } else if (!AuthenticationHelper.isLoggedIn(this)) {
            AlarmlyAPI.disableUserRequests();
        }

        AuthenticationHelper.showCurrentUser(this);
    }

}

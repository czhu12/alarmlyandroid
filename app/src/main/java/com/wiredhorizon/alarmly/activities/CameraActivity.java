package com.wiredhorizon.alarmly.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.recorder.video.AdaptiveSurfaceView;
import com.wiredhorizon.alarmly.recorder.video.CameraHelper;
import com.wiredhorizon.alarmly.recorder.video.VideoRecordingHandler;
import com.wiredhorizon.alarmly.recorder.video.VideoRecordingManager;
import com.wiredhorizon.alarmly.system.DeviceManager;

import java.io.File;
import java.util.List;

/**
 * Created by chriszhu on 14-12-03.
 */
public class CameraActivity extends Activity {
    public static final String ALARM_ID = "ALARM_ID";
    private static String filePath = null;

    private Button recordBtn;
    private ImageButton switchBtn;
    private boolean isInitialized = false;

    private Camera.Size videoSize = null;
    private VideoRecordingManager recordingManager;

    private int alarmId;

    private VideoRecordingHandler recordingHandler = new VideoRecordingHandler() {
        @Override
        public boolean onPrepareRecording() {
            if (!isInitialized) {
                initVideoSize();
                isInitialized = true;
                return true;
            }

            return false;
        }

        @Override
        public Camera.Size getVideoSize() {
            return videoSize;
        }

        @Override
        public int getDisplayRotation() {
            return getWindowManager().getDefaultDisplay().getRotation();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        filePath = getFilePath();

        alarmId = getIntent().getExtras().getInt(ALARM_ID, 0);

        AdaptiveSurfaceView videoView = (AdaptiveSurfaceView) findViewById(R.id.videoView);
        recordingManager = new VideoRecordingManager(videoView, recordingHandler);

        recordBtn = (Button) findViewById(R.id.recordBtn);
        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                record();
            }
        });

        switchBtn = (ImageButton) findViewById(R.id.switchBtn);
        if (recordingManager.getCameraManager().hasMultipleCameras()) {
            switchBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchCamera();
                }
            });
        }
        else {
            switchBtn.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        recordingManager.dispose();
        recordingHandler = null;

        super.onDestroy();
    }

    @SuppressLint("NewApi")
    private void initVideoSize() {
        List<Camera.Size> sizes = CameraHelper.getCameraSupportedVideoSizes(recordingManager.getCameraManager().getCamera());

        videoSize = sizes.get(0);
        recordingManager.setPreviewSize(videoSize);
    }

    @SuppressLint("NewApi")
    private void updateVideoSizes() {
        recordingManager.setPreviewSize(videoSize);
    }

    private void switchCamera() {
        recordingManager.getCameraManager().switchCamera();
        updateVideoSizes();
    }

    private void record() {
        if (recordingManager.stopRecording()) {
            recordBtn.setText("Record");
            switchBtn.setEnabled(true);
            play();
        } else {
            startRecording();
        }
    }

    private void startRecording() {
        if (recordingManager.startRecording(filePath, videoSize)) {
            recordBtn.setText("Stop");
            switchBtn.setEnabled(false);
            return;
        }

        Toast.makeText(this, "Recording error", Toast.LENGTH_LONG).show();
    }

    private void play() {
        Intent i = new Intent(this, PreviewVideoActivity.class);
        i.putExtra(PreviewVideoActivity.FILE_PATH, filePath);
        Log.i("File path", filePath);
        i.putExtra(ALARM_ID, alarmId);
        //startActivityForResult(i, 0);
        startActivity(i);
    }

    public static String getFilePath() {
        File file = CameraHelper.getOutputMediaFile(CameraHelper.MEDIA_TYPE_VIDEO);
        String filename = file.getAbsolutePath();
        return filename;
    }
}

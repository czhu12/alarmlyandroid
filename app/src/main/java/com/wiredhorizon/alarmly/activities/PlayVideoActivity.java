package com.wiredhorizon.alarmly.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.helpers.DownloadTask;
import com.wiredhorizon.alarmly.helpers.GenericCallback;
import com.wiredhorizon.alarmly.models.AlarmMessage;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Chris on 12/18/2014.
 */
public class PlayVideoActivity extends Activity {
    public static final String ALARM_ID = "ALARM_ID";
    private long alarmId;
    List<AlarmMessage> messages;

    @InjectView(R.id.activity_play_video_video_view)
    protected VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        ButterKnife.inject(this);
        alarmId = getIntent().getLongExtra(ALARM_ID, 0);

        initializeVideoView();
        fetchMediaForAlarm();
        //playVid();
    }

    public void playVid() {
        Uri video = Uri.parse("android.resource://com.wiredhorizon.alarmly/raw/test_vid");
        videoView.setVideoURI(video);
        videoView.start();
    }

    public void playVideo(Uri video) {
        DownloadTask downloadTask = new DownloadTask(new GenericCallback() {
            @Override
            public void callback() {

            }

            @Override
            public void callback(String string) {
                Log.i("PlayVideoActivity", "saved video path:"  +string);
                Uri uri = Uri.parse(string);
                videoView.setVideoURI(uri);
                videoView.start();
            }
        });
    }

    // Not totally sure if this is necessary.
    private void initializeVideoView() {
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
    }

    public void fetchMediaForAlarm() {
        AlarmlyAPI api = AlarmlyAPI.getInstance();
        api.getAlarmMedia(this, alarmId, new Callback<List<AlarmMessage>>() {
            @Override
            public void success(List<AlarmMessage> alarmMessages, Response response) {
                // This will get all the media for this alarm. For now we will only use the first one.
                messages = alarmMessages;

                playNextVideo(0);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i("PlayVideoActivity", retrofitError.getMessage() == null ? "" : retrofitError.getMessage());
            }
        });
    }

    public void playNextVideo(final int index) {
        if (index >= messages.size()) {
            Intent i = new Intent(getBaseContext(), MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);

            Log.i("PlayVideoActivity", "Finished videos");
            return;
        }

        final AlarmMessage currentMessage = messages.get(index);
        Log.i("Media format", currentMessage.getFormat());
        if (currentMessage.isVideo()) {
            Uri video = Uri.parse(currentMessage.link);

            Log.i("LINK:", currentMessage.link);
            videoView.setVideoURI(video);
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    AlarmlyAPI.getInstance().viewedMessage(getBaseContext(), currentMessage.id, new Callback<AlarmMessage>() {
                        @Override
                        public void success(AlarmMessage alarmMessage, Response response) {

                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {

                        }
                    });
                    playNextVideo(index + 1);
                }
            });
            videoView.start();
        }
    }
}

package com.wiredhorizon.alarmly.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.wiredhorizon.alarmly.Constants;
import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.adapters.FriendAlarmsAdapter;
import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.helpers.AuthenticationHelper;
import com.wiredhorizon.alarmly.helpers.VideoUploader;
import com.wiredhorizon.alarmly.models.Alarm;
import com.wiredhorizon.alarmly.models.User;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 14-12-11.
 */
public class FriendAlarmsActivity extends Activity {
    @InjectView(R.id.friend_alarms_list)
    ListView friendAlarms;

    private User friend;

    private List<Alarm> alarmsList;
    private FriendAlarmsAdapter adapter;

    private int currentAlarmRemoteID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_alarms);
        ButterKnife.inject(this);
        friend = getIntent().getParcelableExtra(Constants.USER);
        fetchFriendAlarms();
        bindListView();
    }

    private void fetchFriendAlarms() {
        AlarmlyAPI api = AlarmlyAPI.getInstance();
        api.getAlarms(getBaseContext(), friend.id, new Callback<List<Alarm>>() {
            @Override
            public void success(List<Alarm> alarms, Response response) {
                alarmsList.clear();
                alarmsList.addAll(alarms);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });

    }
    private void bindListView() {
        alarmsList = new ArrayList<Alarm>();
        adapter = new FriendAlarmsAdapter(this, 0, alarmsList);
        friendAlarms.setAdapter(adapter);

        friendAlarms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Alarm alarm = (Alarm) view.getTag(R.id.alarm_holder);
                currentAlarmRemoteID = alarm.getRemoteId();
                Log.i("Current Alarm Remote ID", String.valueOf(currentAlarmRemoteID));
                Intent cameraIntent = new Intent(getBaseContext(), CameraActivity.class);
                cameraIntent.putExtra(CameraActivity.ALARM_ID, currentAlarmRemoteID);
                startActivity(cameraIntent);
            }
        });
    }
}

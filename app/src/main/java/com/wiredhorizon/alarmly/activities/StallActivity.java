package com.wiredhorizon.alarmly.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.helpers.AuthenticationHelper;
import com.wiredhorizon.alarmly.models.User;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 14-12-20.
 */
public class StallActivity extends Activity {
    @InjectView(R.id.retry_button)
    Button retryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stall);
        ButterKnife.inject(this);

        createUser();
    }

    private void createUser() {
        AuthenticationHelper.createUser(getApplicationContext(), new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                if (user.id == 0) {return;}

                AuthenticationHelper.login(getBaseContext(), user);
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                AlarmlyAPI.enableUserRequests();
                startActivity(i);
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    @OnClick(R.id.retry_button)
    public void retry() {
        createUser();
    }
}

package com.wiredhorizon.alarmly.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import com.wiredhorizon.alarmly.Constants;
import com.wiredhorizon.alarmly.activities.AlarmReceiverActivity;
import com.wiredhorizon.alarmly.models.Alarm;
import com.wiredhorizon.alarmly.receivers.AlarmReceiver;
import com.wiredhorizon.alarmly.receivers.ResyncReceiver;

import java.util.Calendar;

/**
 * Created by Chris on 11/26/2014.
 */
public class AlarmService extends IntentService {
    public static final String CREATE = "com.wiredhorizon.alarmly.services.CREATE";
    public static final String CANCEL = "com.wiredhorizon.alarmly.services.CANCEL";
    public static final String ALARM_ID = "ALARM_ID";

    private IntentFilter filter;
    public AlarmService() {
        super("NAME");

        filter = new IntentFilter();
        filter.addAction(CREATE);
        filter.addAction(CANCEL);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        long alarmID = intent.getLongExtra(ALARM_ID, -1);
        if (filter.matchAction(action) && alarmID > -1) {
            execute(action, alarmID);
        }
    }

    private void execute(String action, long alarmID) {
        Alarm alarm = Alarm.find(alarmID);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.set(Calendar.HOUR_OF_DAY, alarm.getMilitaryHour());
        calendar.set(Calendar.MINUTE, alarm.getMilitaryMinute());
        Log.i("Alarm Minute:", "" + alarm.getMilitaryMinute());
        Log.i("Alarm hour:", "" + alarm.getMilitaryHour());

        // We are looking to use the RTC_WAKEUP
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(AlarmReceiverActivity.ALARM_ID, alarmID);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        // So, here is the problem, if the time is in the past, it will be called immediately.
        // We need to figure out if the time is past this and then schedule it for the next day.
        long alarmTimeAdjustedForCurrentTime = calcAlarmTimeAdjustedForCurrentTime(calendar.getTimeInMillis());

        if (action.equals(CREATE)) {
            Log.i("Alarm Scheduling", "Scheduled alarm for : " + calendar.getTimeInMillis());
            Log.i("Alarm Scheduling", "Current Time: " + System.currentTimeMillis());

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTimeAdjustedForCurrentTime, Constants.DAY, alarmIntent);
        } else if (action.equals(CANCEL)) {
            Log.i("Alarm Scheduling", "Unscheduled alarm for : " + alarmTimeAdjustedForCurrentTime);
            alarmManager.cancel(alarmIntent);
            if (!alarm.existsRemotely() && alarm.isDeleted()) {
                alarm.delete();
            }
        }
    }

    private long calcAlarmTimeAdjustedForCurrentTime(long unadjustedTime) {
        if (unadjustedTime < System.currentTimeMillis()) {
            unadjustedTime += Constants.DAY;
        }
        return unadjustedTime;
    }

    public static void scheduleInterval(Context context) {
        Intent i = new Intent(context, ResyncReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, Constants.REQUEST_SYNC, i, 0);

        long firstTime = SystemClock.elapsedRealtime();
        firstTime += 3 * Constants.SECOND;

        AlarmManager am = (AlarmManager) context
                .getSystemService(ALARM_SERVICE);

        //5min interval

        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime,
                Constants.SYNC_INTERVAL, sender);
    }

    public static void cancelInterval(Context context) {
        Intent i = new Intent(context, ResyncReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, Constants.REQUEST_SYNC, i, 0);
        AlarmManager am = (AlarmManager) context
                .getSystemService(ALARM_SERVICE);

        am.cancel(sender);
    }

    public static void rescheduleInterval(Context context) {
        cancelInterval(context);
        scheduleInterval(context);
    }
}

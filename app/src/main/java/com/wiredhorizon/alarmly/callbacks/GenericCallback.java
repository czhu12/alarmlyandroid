package com.wiredhorizon.alarmly.callbacks;

/**
 * Created by chriszhu on 14-11-26.
 */
public interface GenericCallback {
    public void callback();
}

package com.wiredhorizon.alarmly.api;


import com.activeandroid.annotation.Column;
import com.google.gson.annotations.SerializedName;
import com.wiredhorizon.alarmly.models.Alarm;

import java.sql.Date;

/**
 * Created by Chris on 12/7/2014.
 */
public class AlarmDAO {
    @SerializedName("id")
    private int id;

    @SerializedName("local_id")
    private long localId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("minute")
    private int mMilitaryMinute;

    @SerializedName("hour")
    private int mMilitaryHour;

    @SerializedName("active")
    private boolean mActive;

    @SerializedName("is_synced")
    private boolean isSynced;

    @SerializedName("updated_at_time")
    private Date updatedAt;

    public AlarmDAO(String title, int militaryHour, int militaryMinute, boolean active, boolean isSynced, Date updatedAt, long localId) {
        this.mTitle = title;
        this.mActive = active;
        this.mMilitaryHour = militaryHour;
        this.mMilitaryMinute = militaryMinute;
        this.isSynced = isSynced;
        this.updatedAt = updatedAt;
        this.localId = localId;
    }

    public Alarm toAlarm() {
        return Alarm.find(localId);
    }

    public Alarm toTempAlarm() {
        return new Alarm(id, mTitle, mMilitaryHour, mMilitaryMinute, mActive);
    }

    public int getRemoteId() {
        return id;
    }
}

package com.wiredhorizon.alarmly.api.dao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chriszhu on 15-01-07.
 */
public class TranslationDAO {
    @SerializedName("phone_numbers")
    List<String> phoneNumbers;
    public TranslationDAO() {
        phoneNumbers = new ArrayList<String>();
    }

    public TranslationDAO(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public void addPhoneNumber(String phoneNumber) {
        phoneNumbers.add(phoneNumber);
    }
}

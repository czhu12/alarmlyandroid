package com.wiredhorizon.alarmly.api;

import com.wiredhorizon.alarmly.api.dao.StatusDAO;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by chriszhu on 14-12-12.
 */
public interface FileUploader {
    @Multipart
    @POST("/alarms/{alarm_id}/messages")
    void upload(
            @Query("access_token") String accessToken,
            @Path("alarm_id") int alarmId,
            @Part("media") TypedFile video,
            @Part("format") String type,
            Callback<StatusDAO> callback
    );
}

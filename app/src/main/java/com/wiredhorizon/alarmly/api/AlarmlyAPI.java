package com.wiredhorizon.alarmly.api;

import android.content.Context;

import com.wiredhorizon.alarmly.Constants;
import com.wiredhorizon.alarmly.api.dao.TranslationDAO;
import com.wiredhorizon.alarmly.helpers.AuthenticationHelper;
import com.wiredhorizon.alarmly.models.Alarm;
import com.wiredhorizon.alarmly.models.AlarmMessage;
import com.wiredhorizon.alarmly.models.Contact;
import com.wiredhorizon.alarmly.models.User;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Chris on 12/7/2014.
 */
public class AlarmlyAPI {
    private static AlarmlyAPI ourInstance = new AlarmlyAPI();
    private RestAdapter restAdapter;
    private UsersAPI usersAPICached;
    private AlarmsAPI alarmsAPICached;
    private FriendsAPI friendsAPICached;
    private AuthenticationAPI authenticationAPICached;
    private MessagesAPI messagesAPICached;
    private TranslationAPI translationAPICached;

    private static boolean userRequestsEnabled = true;

    public static AlarmlyAPI getInstance() {
        return ourInstance;
    }

    private AlarmlyAPI() {
        restAdapter = new RestAdapter.Builder().setEndpoint(Constants.API).build();
        alarmsAPICached = restAdapter.create(AlarmsAPI.class);
        usersAPICached = restAdapter.create(UsersAPI.class);
        authenticationAPICached = restAdapter.create(AuthenticationAPI.class);
        friendsAPICached = restAdapter.create(FriendsAPI.class);
        messagesAPICached = restAdapter.create(MessagesAPI.class);
        translationAPICached = restAdapter.create(TranslationAPI.class);
    }

    public void getUsers(Context context, Callback<List<User>> callback) {
        if (!userRequestsEnabled) return;

        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        usersAPICached.getUsers(accessToken, callback);
    }

    public void showUser(Context context, Callback<User> callback) {
        if (!userRequestsEnabled) return;
        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        int userID = AuthenticationHelper.getCurrentUserID(context);
        usersAPICached.showUser(userID, accessToken, callback);
    }

    public void createUser(User user, Callback<User> callback) {
        usersAPICached.createUser(user, callback);
    }

    public void getAlarmMedia(Context context, long alarmId, Callback<List<AlarmMessage>> callback) {
        if (!userRequestsEnabled) return;

        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        messagesAPICached.getAlarmMedia(alarmId, accessToken, callback);
    }

    public void getAlarms(Context context, int userId, final Callback<List<Alarm>> callback) {
        if (!userRequestsEnabled) return;

        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        Callback<List<AlarmDAO>> alarmDAOCallback = new Callback<List<AlarmDAO>>() {
            @Override
            public void success(List<AlarmDAO> alarmDAOs, Response response) {
                List<Alarm> alarms = new ArrayList<Alarm>();
                for (AlarmDAO alarmDAO: alarmDAOs) {
                    alarms.add(alarmDAO.toTempAlarm());
                }
                callback.success(alarms, response);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                callback.failure(retrofitError);
            }
        };

        alarmsAPICached.getAlarms(userId, accessToken, alarmDAOCallback);
    }

    public void createAlarm(Context context, Alarm alarm, final Callback<Alarm> callback) {
        if (!userRequestsEnabled) return;

        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        int userID = AuthenticationHelper.getCurrentUserID(context);

        Callback<AlarmDAO> alarmDAOCallback = new Callback<AlarmDAO>() {
            @Override
            public void success(AlarmDAO alarmDAO, Response response) {
                Alarm alarm = alarmDAO.toAlarm();
                alarm.setRemoteId(alarmDAO.getRemoteId());
                // Here, we have to update the alarm.
                callback.success(alarm, response);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                callback.failure(retrofitError);
            }
        };

        alarmsAPICached.createAlarm(userID, accessToken, alarm.toAlarmDAO(), alarmDAOCallback);
    }

    public void getFriends(Context context, Callback<List<User>> callback) {
        if (!userRequestsEnabled) return;

        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        int userID = AuthenticationHelper.getCurrentUserID(context);
        friendsAPICached.getFriends(userID, accessToken, callback);
    }

    public void addFriend(Context context, int friendID, Callback<User> callback) {
        if (!userRequestsEnabled) return;

        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        int userID = AuthenticationHelper.getCurrentUserID(context);
        friendsAPICached.addFriend(userID, accessToken, friendID, callback);
    }

    public void login(User user, Callback<User> callback) {
        authenticationAPICached.login(user, callback);
    }

    public void updateAlarm(Context context, Alarm item, final Callback<Alarm> callback) {
        if (!userRequestsEnabled) return;

        int remoteId = item.getRemoteId();
        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        int userID = AuthenticationHelper.getCurrentUserID(context);
        AlarmDAO alarmDAO = item.toAlarmDAO();

        alarmsAPICached.updateAlarm(remoteId, accessToken, alarmDAO, new Callback<AlarmDAO>() {
            @Override
            public void success(AlarmDAO alarmDAO, Response response) {
                Alarm alarm = alarmDAO.toAlarm();
                callback.success(alarm, response);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                callback.failure(retrofitError);
            }
        });
    }

    public void deleteAlarm(Context context, Alarm item, final Callback<Alarm> callback) {
        if (!userRequestsEnabled) return;

        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        int userID = AuthenticationHelper.getCurrentUserID(context);
        alarmsAPICached.deleteAlarm(item.getRemoteId(), accessToken, new Callback<AlarmDAO>() {
            @Override
            public void success(AlarmDAO alarmDAO, Response response) {
                Alarm alarm = alarmDAO.toAlarm();
                callback.success(alarm, response);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                callback.failure(retrofitError);
            }
        });
    }

    public void translateContacts(Context context, List<Contact> contacts, Callback<List<User>> callback) {
        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        TranslationDAO dao = new TranslationDAO();

        for (Contact contact : contacts) {
            dao.addPhoneNumber(contact.getPhoneNumber());
        }

        translationAPICached.translation(accessToken, dao, callback);
    }

    public static void disableUserRequests() {
        userRequestsEnabled = false;
    }

    public static void enableUserRequests() {
        userRequestsEnabled = true;
    }

    public void viewedMessage(Context context, int messageId, Callback<AlarmMessage> callback) {
        String accessToken = AuthenticationHelper.getAlarmlyAccessToken(context);
        AlarmMessage message = new AlarmMessage();
        message.viewed = true;
        messagesAPICached.updateMessage(messageId, accessToken, message, callback);
    }


    public interface UsersAPI {
        @GET("/users")
        public void getUsers(@Query("access_token") String accessToken, Callback<List<User>> users);

        @GET("/users/{id}")
        public void showUser(@Path("id") int id, @Query("access_token") String accessToken, Callback<User> users);

        @POST("/users")
        public void createUser(@Body User user, Callback<User> users);
    }

    // TODO: figure out what to do with the callback generic type.
    public interface AlarmsAPI {
        @GET("/users/{id}/alarms")
        public void getAlarms(@Path("id") int userID, @Query("access_token") String accessToken, Callback<List<AlarmDAO>> callback);

        @POST("/users/{id}/alarms")
        public void createAlarm(@Path("id") int userID, @Query("access_token") String accessToken, @Body AlarmDAO alarmDAO, Callback<AlarmDAO> callback);

        @PUT("/alarms/{remoteId}")
        public void updateAlarm(@Path("remoteId") int remoteId, @Query("access_token") String accessToken, @Body AlarmDAO alarmDAO, Callback<AlarmDAO> callback);

        @DELETE("/alarms/{remoteId}")
        public void deleteAlarm(@Path("remoteId") int remoteId, @Query("access_token") String accessToken, Callback<AlarmDAO> callback);
    }

    public interface MessagesAPI {
        @GET("/alarms/{id}/messages")
        public void getAlarmMedia(@Path("id") long alarmId, @Query("access_token") String accessToken, Callback<List<AlarmMessage>> callback);

        @PUT("/messages/{id}")
        public void updateMessage(@Path("id") int messageId, @Query("access_token") String accessToken, @Body AlarmMessage message, Callback<AlarmMessage> callback);
    }

    public interface FriendsAPI {
        @GET("/users/{id}/friendships")
        public void getFriends(@Path("id") int userID, @Query("access_token") String accessToken, Callback<List<User>> callback);

        @POST("/users/{id}/friendships")
        public void addFriend(@Path("id") int userID, @Query("access_token") String accessToken, @Query("friend_id") int friendID, Callback<User> callback);
    }

    public interface AuthenticationAPI {
        @POST("/sessions")
        public void login(@Body User user, Callback<User> callback);
    }

    public interface TranslationAPI {
        @POST("/users/translation")
        public void translation(@Query("access_token") String accessToken, @Body TranslationDAO body, Callback<List<User>> callback);
    }
}

package com.wiredhorizon.alarmly.api;

import android.content.Context;
import android.util.Log;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 14-12-11.
 */
public class SyncManager<T> {
    private Context context;
    private SyncAdapter<T> syncAdapter;

    public SyncManager(Context context, SyncAdapter<T> syncAdapter) {
        this.context = context;
        this.syncAdapter = syncAdapter;
    }

    public void sync() {
        List<T> itemsToSync = this.syncAdapter.getSyncItems();
        for (T item : itemsToSync) {
            // oldItem never changes does it?
            final T oldItem = item;
            syncAdapter.syncItem(item, new Callback<T>() {
                @Override
                public void success(T newItem, Response response) {
                    if (syncAdapter.isCorrectlySynced(oldItem, newItem)) {
                        syncAdapter.itemSynced(oldItem);
                    } else {
                        syncAdapter.itemSyncFailed(oldItem);
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                        syncAdapter.itemSyncFailed(oldItem);
                }
            });
        }
    }
}

package com.wiredhorizon.alarmly.api;

import android.content.Context;
import android.util.Log;

import com.wiredhorizon.alarmly.models.Alarm;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 14-12-11.
 */
public class AlarmSyncAdapter extends SyncAdapter<Alarm> {
    private Context context;

    public AlarmSyncAdapter(Context context) {
        this.context = context;
    }

    @Override
    public List<Alarm> getSyncItems() {
        Alarm.printSync();
        return Alarm.getAlarmsToSync();
    }

    @Override
    public void itemSynced(Alarm item) {
        item.setIsSynced(true);
        item.save();
    }

    @Override
    public void itemSyncFailed(Alarm item) {
        // Do nothing
    }

    @Override
    public void syncItem(final Alarm item, final Callback<Alarm> callback) {
        AlarmlyAPI api = AlarmlyAPI.getInstance();
        // What if it is created, then immediately deleted?
        // In this case, we could create the alarm on the server, then delete it the next round.
        // or we can just let it fall through.
        // We will let it fall through.

        if (item.existsRemotely() && !item.isDeleted()) {
            updateAlarm(api, item, callback);
        } else if (item.existsRemotely() && item.isDeleted()) {
           deleteAlarm(api, item, callback);
        } else if (!item.existsRemotely() && !item.isDeleted()) {
            createAlarm(api, item, callback);
        } else if (!item.existsRemotely() && item.isDeleted()) {
            // This means that an alarm was created, and deleted immediately. Don't even need this case.
            item.delete();
        }
    }

    private void updateAlarm(AlarmlyAPI api, Alarm item, final Callback<Alarm> callback) {
        Log.i("Alarm sync:", "updating");
        api.updateAlarm(context, item, callback);
    }

    private void createAlarm(AlarmlyAPI api, final Alarm item, final Callback<Alarm> callback) {
        Log.i("Alarm sync:", "creating");
        api.createAlarm(context, item, new Callback<Alarm>() {
            @Override
            public void success(Alarm alarm, Response response) {
                item.setRemoteId(alarm.getRemoteId());
                Log.i("new remote id", String.valueOf(alarm.getRemoteId()));
                callback.success(alarm, response);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i("AlarmSyncAdapter", "Create sync failed" + retrofitError.getMessage());
            }
        });
    }

    private void deleteAlarm(AlarmlyAPI api, Alarm item, final Callback<Alarm> callback) {
        Log.i("Alarm sync:", "deleting");

        api.deleteAlarm(context, item, new Callback<Alarm>() {
            @Override
            public void success(Alarm alarm, Response response) {
                alarm.delete();
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    @Override
    public boolean isCorrectlySynced(Alarm oldAlarm, Alarm newAlarm) {
        Log.i("Is synced correctly", String.valueOf(newAlarm.getUpdatedAt() == oldAlarm.getUpdatedAt()));
        if (newAlarm.getUpdatedAt() == oldAlarm.getUpdatedAt()) {
            Log.i("Sync logic", "sync accepted");
        } else {
            Log.i("Sync logic", "sync rejected");
        }

        return newAlarm.getUpdatedAt() == oldAlarm.getUpdatedAt();
    }
}

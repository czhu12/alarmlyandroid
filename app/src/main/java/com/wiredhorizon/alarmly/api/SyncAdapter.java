package com.wiredhorizon.alarmly.api;

import java.util.List;

import retrofit.Callback;

/**
 * Created by chriszhu on 14-12-11.
 */
public abstract class SyncAdapter<T> {
    abstract public List<T> getSyncItems();

    abstract public void itemSynced(T item);

    abstract public void itemSyncFailed(T item);

    abstract public void syncItem(T item, Callback<T> callback);

    abstract public boolean isCorrectlySynced(T oldItem, T newItem);
}

package com.wiredhorizon.alarmly.api.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chriszhu on 14-12-14.
 */
public class StatusDAO {
    @SerializedName("message") String message;
}

package com.wiredhorizon.alarmly.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.wiredhorizon.alarmly.api.AlarmSyncAdapter;
import com.wiredhorizon.alarmly.api.SyncManager;
import com.wiredhorizon.alarmly.system.DeviceManager;

/**
 * Created by chriszhu on 14-12-11.
 */
public class ResyncReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("ResyncReceiver", "resyncing...");
        DeviceManager deviceManager = new DeviceManager();

        if (deviceManager.isNetworkConnected(context)) {
            SyncManager syncManager = new SyncManager(context, new AlarmSyncAdapter(context));
            syncManager.sync();
        }
    }
}

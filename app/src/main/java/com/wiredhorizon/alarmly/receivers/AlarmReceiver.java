package com.wiredhorizon.alarmly.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.wiredhorizon.alarmly.activities.AlarmReceiverActivity;
import com.wiredhorizon.alarmly.services.AlarmService;

/**
 * Created by Chris on 11/27/2014.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private long alarmId;
    @Override
    public void onReceive(Context context, Intent intent) {
        alarmId = intent.getLongExtra(AlarmService.ALARM_ID, 0);

        Intent i = new Intent(context, AlarmReceiverActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(AlarmReceiverActivity.ALARM_ID, alarmId);
        context.startActivity(i);

    }
}

package com.wiredhorizon.alarmly.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wiredhorizon.alarmly.R;

import butterknife.ButterKnife;

/**
 * Created by chriszhu on 14-12-11.
 */
public class FriendAlarmsFragment extends Fragment {

    public static FriendAlarmsFragment newInstance() {
        return new FriendAlarmsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_friends_alarms, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }
}

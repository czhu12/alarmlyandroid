package com.wiredhorizon.alarmly.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.adapters.AlarmsAdapter;
import com.wiredhorizon.alarmly.callbacks.GenericCallback;
import com.wiredhorizon.alarmly.helpers.AlarmStyleManager;
import com.wiredhorizon.alarmly.helpers.SwipeDismissListViewTouchListener;
import com.wiredhorizon.alarmly.models.Alarm;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by chriszhu on 14-11-22.
 */
public class AlarmsFragment extends Fragment {
    @InjectView(R.id.fragment_alarms_create_header)
    RelativeLayout createHeader;

    @InjectView(R.id.alarms_list_view)
    ListView alarmsListView;
    @InjectView(R.id.fragment_alarms_create_alarm)
    ImageButton createButton;

    private List<Alarm> alarmsList;
    private AlarmsAdapter alarmsAdapter;
    private AlarmStyleManager alarmStyleManager;

    public static AlarmsFragment newInstance() {
        return new AlarmsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_alarms, container, false);
        ButterKnife.inject(this, rootView);
        bindList();
        repopulateAlarmsList();
        bindCreateButton();
        createSwipeToDismiss();

        alarmsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                updateHeaderColor();
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {

            }
        });
        return rootView;
    }

    private void updateHeaderColor() {
        if (alarmStyleManager == null) {
            alarmStyleManager = new AlarmStyleManager(getActivity());
        }
        if (alarmsList.size() == 0) {
            createHeader.setBackgroundColor(getResources().getColor(R.color.belize_hole));
            return;
        }

        int index = alarmsListView.getFirstVisiblePosition();
        Alarm alarm = alarmsList.get(index);
        int color = alarmStyleManager.getColor(alarm);
        createHeader.setBackgroundColor(color);
    }

    public void bindCreateButton() {
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateAlarmFragment fragment = CreateAlarmFragment.newInstance();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                fragment.setOnCreateButtonClickListener(new GenericCallback() {
                    @Override
                    public void callback() {
                        repopulateAlarmsList();
                    }
                });

                fragment.show(ft, "dialog");
            }
        });
    }

    public void bindList() {
        alarmsList = new ArrayList<Alarm>();
        alarmsAdapter = new AlarmsAdapter(getActivity(), 0, alarmsList);
        alarmsListView.setAdapter(alarmsAdapter);
        alarmsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("TAG", "trying to show something");
                EditAlarmFragment fragment = EditAlarmFragment.newInstance(alarmsList.get(i).getId());
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                fragment.setOnCreateButtonClickListener(new GenericCallback() {
                    @Override
                    public void callback() {
                        repopulateAlarmsList();
                    }
                });

                fragment.show(ft, "dialog");
            }
        });
    }

    public void repopulateAlarmsList() {
        List<Alarm> alarms = Alarm.index();
        Alarm.sortAlarms(alarms);
        alarmsList.clear();
        alarmsList.addAll(alarms);
        alarmsAdapter.notifyDataSetChanged();
        updateHeaderColor();
    }

    public void createSwipeToDismiss() {
        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        alarmsListView,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    Alarm alarm = alarmsAdapter.getItem(position);
                                    alarm.scheduleForDelete(getActivity());
                                    alarmsAdapter.remove(alarm);
                                }
                                alarmsAdapter.notifyDataSetChanged();
                            }
                        });
        alarmsListView.setOnTouchListener(touchListener);
        alarmsListView.setOnScrollListener(touchListener.makeScrollListener());
    }
}
package com.wiredhorizon.alarmly.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.wiredhorizon.alarmly.Constants;
import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.activities.FriendAlarmsActivity;
import com.wiredhorizon.alarmly.activities.MainActivity;
import com.wiredhorizon.alarmly.adapters.FriendsAdapter;
import com.wiredhorizon.alarmly.adapters.HomeFragmentAdapter;
import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.helpers.ContactsTranslatorService;
import com.wiredhorizon.alarmly.models.Contact;
import com.wiredhorizon.alarmly.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 14-11-22.
 */
public class FriendsFragment extends Fragment {
    @InjectView(R.id.friends_list_view)
    ListView friendsListView;

    private FriendsAdapter adapter;
    private List<User> friends;
    private ContactsTranslatorService contactsTranslatorService;

    public FriendsFragment() {
    }

    public static FriendsFragment newInstance() {
        FriendsFragment fragment = new FriendsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_friends, container, false);
        ButterKnife.inject(this, rootView);
        bindListView();
        contactsTranslatorService = new ContactsTranslatorService(getActivity().getApplicationContext());
        populateFriends();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void bindListView() {
        friends = new ArrayList<User>();
        adapter = new FriendsAdapter(getActivity(), 0, friends);
        friendsListView.setAdapter(adapter);
        friendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), FriendAlarmsActivity.class);
                User friend = (User) view.getTag(R.id.user_holder);
                intent.putExtra(Constants.USER, friend);
                startActivity(intent);
            }
        });

    }

    private void populateFriends() {

        AlarmlyAPI api = AlarmlyAPI.getInstance();
        api.getFriends(getActivity(), new Callback<List<User>>() {
            @Override
            public void success(List<User> users, Response response) {
                friends.clear();
                friends.addAll(users);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i("Request not successful", "fail" + retrofitError.getMessage());
            }
        });

        contactsTranslatorService.translate(new Callback<List<User>>() {
            @Override
            public void success(List<User> users, Response response) {
                // these are the users.
                friends.addAll(users);
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }
}

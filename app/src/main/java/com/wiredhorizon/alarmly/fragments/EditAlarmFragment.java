package com.wiredhorizon.alarmly.fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.callbacks.GenericCallback;
import com.wiredhorizon.alarmly.models.Alarm;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by chriszhu on 14-11-26.
 */
public class EditAlarmFragment extends DialogFragment {
    public static final String EDIT_ALARM_STRING = "EDIT_ALARM_STRING";

    private Alarm alarm;
    @InjectView(R.id.fragment_edit_alarm_cancel)
    Button cancelButton;

    @InjectView(R.id.fragment_edit_alarm_save)
    Button saveButton;

    @InjectView(R.id.fragment_edit_alarm_time_picker)
    TimePicker timePicker;

    private GenericCallback callback;
    public static EditAlarmFragment newInstance(long alarmID) {
        EditAlarmFragment fragment = new EditAlarmFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(EDIT_ALARM_STRING, alarmID);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_alarm, container, false);
        ButterKnife.inject(this, rootView);

        long alarmID = getArguments().getLong(EDIT_ALARM_STRING);
        alarm = Alarm.find(alarmID);
        buildView();
        return rootView;
    }

    public void buildView() {
        getDialog().setTitle("Edit Alarm");
        timePicker.setCurrentHour(alarm.getMilitaryHour());
        timePicker.setCurrentMinute(alarm.getMilitaryMinute());
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alarm.updateWithMilitaryTime(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
                alarm.save();
                if (callback != null) callback.callback();
                dismiss();
            }
        });
    }

    public void setOnCreateButtonClickListener(GenericCallback genericCallback) {
        this.callback = genericCallback;
    }
}

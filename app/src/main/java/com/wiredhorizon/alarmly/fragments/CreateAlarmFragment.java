package com.wiredhorizon.alarmly.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.wiredhorizon.alarmly.R;
import com.wiredhorizon.alarmly.activities.MainActivity;
import com.wiredhorizon.alarmly.api.AlarmlyAPI;
import com.wiredhorizon.alarmly.callbacks.GenericCallback;
import com.wiredhorizon.alarmly.models.Alarm;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnTextChanged;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chriszhu on 14-11-26.
 */
public class CreateAlarmFragment extends DialogFragment {
    @InjectView(R.id.fragment_create_alarm_title_error)
    protected TextView alarmTitleError;

    @InjectView(R.id.fragment_create_alarm_title)
    protected EditText alarmTitle;

    @InjectView(R.id.fragment_create_alarm_cancel)
    protected Button cancelButton;

    @InjectView(R.id.fragment_create_alarm_create)
    protected Button createButton;

    @InjectView(R.id.fragment_create_alarm_time_picker)
    protected TimePicker timePicker;

    protected GenericCallback createButtonCallback;
    public static CreateAlarmFragment newInstance() {
        return new CreateAlarmFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_alarm, container, false);

        ButterKnife.inject(this, rootView);
        bindExitButtons();
        getDialog().setTitle("Create Alarm");
        return rootView;
    }

    public void bindExitButtons() {
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passesValidation()) {
                    saveAlarm();
                    if (createButtonCallback != null) createButtonCallback.callback();
                    dismiss();
                } else {
                    showValidationErrors();
                }
            }
        });
    }

    private void showValidationErrors() {
        if (!titleIsCorrect()) {
            // here we want to highlight the title
            alarmTitle.setBackground(getActivity().getResources().getDrawable(R.drawable.error_highlight));
            alarmTitleError.setVisibility(View.VISIBLE);
        }
    }

    private boolean passesValidation() {
        return titleIsCorrect();
    }

    private boolean titleIsCorrect() {
        String title = alarmTitle.getText().toString();
        return !title.trim().equals("");
    }

    public void saveAlarm() {
        Log.i("Save alarm", "saving alarm...");
        int militaryMinute = timePicker.getCurrentMinute();
        int militaryHour = timePicker.getCurrentHour();
        String title = alarmTitle.getText().toString();
        Alarm alarm = Alarm.fromMilitaryTime(title, militaryHour, militaryMinute, true);

        alarm.saveAndSetAlarm(getActivity());
    }

    public void setOnCreateButtonClickListener(GenericCallback callback) {
        this.createButtonCallback = callback;
    }

    @OnTextChanged(R.id.fragment_create_alarm_title)
    void onTextChanged(CharSequence text) {
        alarmTitle.setBackground(null);
    }
}

package com.wiredhorizon.alarmly;

import android.app.Application;
import android.content.Intent;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.wiredhorizon.alarmly.models.Alarm;
import com.wiredhorizon.alarmly.models.User;
import com.wiredhorizon.alarmly.services.AlarmService;

/**
 * Created by chriszhu on 14-11-26.
 */
public class AlarmlyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Configuration.Builder config = new Configuration.Builder(this);
        config.setDatabaseVersion(5);

        Intent i = new Intent(this, AlarmService.class);
        startService(i);

        ActiveAndroid.initialize(config.create());
    }
}

package com.wiredhorizon.alarmly;

/**
 * Created by Chris on 12/6/2014.
 */
public class Constants {
    public static final String CURRENT_USER = "CURRENT_USER";
    public static final String API = "http://104.236.100.179:3000";
        public static final String UPLOAD = "http://104.236.100.179:3000";
    public static final String HOST = "http://104.236.100.179:3000";


    public static final boolean SHOULD_VERIFY_LOGGED_IN = true;
    public static final String USER = "USER";
    public static final int REQUEST_SYNC = 1000;

    public static final int SECOND = 1000;
    public static final int MINUTE = 60 * SECOND;
    public static final int HOUR = 60 * MINUTE;
    public static final int DAY = 24 * HOUR;
    public static final int WEEK = 7 * DAY;

    public static final long SYNC_INTERVAL = 1 * MINUTE;
}
